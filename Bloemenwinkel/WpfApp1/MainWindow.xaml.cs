﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        

        private void Rozen_MouseEnter(object sender, MouseEventArgs e)
        {
            Background.Background = Brushes.Red;
        }

        private void Tulpen_MouseEnter(object sender, MouseEventArgs e)
        {
            Background.Background = Brushes.Blue;
        }

        private void Lavendel_MouseEnter(object sender, MouseEventArgs e)
        {
            Background.Background = Brushes.Purple;
        }

        private void Meiklokje_MouseEnter(object sender, MouseEventArgs e)
        {
            Background.Background = Brushes.White;
        }

        private void Rozen_MouseLeave(object sender, MouseEventArgs e)
        {
            Background.Background = Brushes.Gray;
        }
    }
}
